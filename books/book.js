const mongoose = require('mongoose');

// Model (reference to collection)
mongoose.model('Book', {
  // Title, author, pages, publisher
  title: {
    type: String,
    required: true,
  },
  author: {
    type: String,
    required: true,
  },
  pages: {
    type: Number,
    required: false,
  },
  publisher: {
    type: String,
    required: false,
  },
});