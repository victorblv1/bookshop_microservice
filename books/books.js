// Loading express.
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

app.use(bodyParser.json());

// Connecting to bookservice db.
mongoose.connect('mongodb://localhost:27017/bookservice',
    {useNewUrlParser: true}, () => {
      console.log('Book Database is connected.');
    });

// Load Book model.
require('./book');
const Book = mongoose.model('Book');

app.get('/', (req, res) => {
  res.send('Main endpoint for the bookservice.');
});

// Create func.
app.post('/book', (req, res) => {
  var newBook = {
    title: req.body.title,
    author: req.body.author,
    pages: req.body.pages,
    publisher: req.body.publisher,
  };

  var book = new Book(newBook);
  book.save().then(() => {
    console.log('New book created.');
  }).catch((err) => {
    if (err) {
      throw err;
    }
  });
  res.send('A new book have been created successfully.');
});

app.get('/books', (req, res) => {
  Book.find().then((books) => {
    res.json(books);
  }).catch(err => {
    if (err) {
      throw err;
    }
  });
});

app.get('/book/:id', (req, res) => {
  Book.findById(req.params.id).then((book) => {
    if (book) {
      res.json(book);
    } else {
      res.sendStatus(404);
    }
  }).catch(err => {
    if (err) {
      throw err;
    }
  });
});

app.delete('/book/:id', (req, res) => {
  Book.findByIdAndRemove(req.params.id).then(() => {
    res.send('Book has been deleted successfully.');
  }).catch(err => {
    if (err) {
      throw err;
    }
  });
});

app.listen(4545, () => {
  console.log('Books service Up and Running ...');
});