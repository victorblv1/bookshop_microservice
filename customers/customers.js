const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

app.use(bodyParser.json());

// Connect to customer db.
mongoose.connect('mongodb://localhost:27017/customerservice',
    {useNewUrlParser: true}, () => {
      console.log('Customer Database is connected.');
    });

// Load Customer model.
require('./customer');
const Customer = mongoose.model('Customer');

app.get('/', (req, res) => {
  res.send('Main endpoint for the customerservice.');
});

app.post('/customer', (req, res) => {
  var newCustomer = {
    name: req.body.name,
    age: req.body.age,
    address: req.body.address,
  };

  var customer = new Customer(newCustomer);
  customer.save().then(() => {
    console.log('New customer created.');
  }).catch((err) => {
    if (err) {
      throw err;
    }
  });
  res.send('A new customer have been created successfully.');
});

app.get('/customers', (req, res) => {
  Customer.find().then((customers) => {
    res.json(customers);
  }).catch(err => {
    if (err) {
      throw err;
    }
  });
});

app.get('/customer/:id', (req, res) => {
  Customer.findById(req.params.id).then((customer) => {
    if (customer) {
      res.json(customer);
    } else {
      res.sendStatus(404);
    }
  }).catch(err => {
    if (err) {
      throw err;
    }
  });
});

app.delete('/customer/:id', (req, res) => {
  Customer.findByIdAndRemove(req.params.id).then(() => {
    res.send('Customer has been deleted successfully.');
  }).catch(err => {
    if (err) {
      throw err;
    }
  });
});

app.listen(5555, () => {
  console.log('Customers service Up and Running ...');
});
