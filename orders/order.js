const mongoose = require('mongoose');

mongoose.model('Order', {
  customerId: {
    type: mongoose.SchemaTypes.ObjectID,
    required: true,
  },
  bookId: {
    type: mongoose.SchemaTypes.ObjectID,
    required: true,
  },
  initialDate: {
    type: Date,
    required: true,
  },
  deliveryDate: {
    type: Date,
    required: true,
  },
});