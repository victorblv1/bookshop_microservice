const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const axios = require('axios');

app.use(bodyParser.json());

// Connect to orders db.
mongoose.connect('mongodb://localhost:27017/orderservice',
    {useNewUrlParser: true}, () => {
      console.log('Order Database is connected.');
    });

// Load Order model.
require('./order');
const Order = mongoose.model('Order');

app.get('/', (req, res) => {
  res.send('Main endpoint for the orderservice.');
});

app.post('/order', (req, res) => {
  var newOrder = {
    customerId: mongoose.Types.ObjectId(req.body.customerId),
    bookId: mongoose.Types.ObjectId(req.body.bookId),
    initialDate: req.body.initialDate,
    deliveryDate: req.body.deliveryDate,
  };

  var order = new Order(newOrder);
  order.save().then(() => {
    console.log('New order created.');
  }).catch((err) => {
    if (err) {
      throw err;
    }
  });
  res.send('A new order have been created successfully.');
});

app.get('/orders', (req, res) => {
  Order.find().then((orders) => {
    res.json(orders);
  }).catch(err => {
    if (err) {
      throw err;
    }
  });
});

// Return order information (id's).
// app.get('/order/:id', (req, res) => {
//   Order.findById(req.params.id).then((order) => {
//     if (order) {
//       res.json(order);
//     } else {
//       res.sendStatus(404);
//     }
//   }).catch(err => {
//     if (err) {
//       throw err;
//     }
//   });
// });

// Return name of customer & book title.
app.get('/order/:id', (req, res) => {
  Order.findById(req.params.id).then((order) => {
    if (order) {
      axios.get('http://localhost:5555/customer/' + order.customerId).
      then((response) => {
        var orderObj = {customerName: response.data.name, bookTitle: ''};
        axios.get('http://localhost:4545/book/' + order.bookId).
        then((response) => {
          orderObj.bookTitle = response.data.title;
          res.json(orderObj);
        });
      });
    } else {
      res.send('Invalid Order.');
    }
  }).catch(err => {
    if (err) {
      throw err;
    }
  });
});

app.delete('/order/:id', (req, res) => {
  Order.findByIdAndRemove(req.params.id).then(() => {
    res.send('Order has been deleted successfully.');
  }).catch(err => {
    if (err) {
      throw err;
    }
  });
});

app.listen(7777, () => {
  console.log('Orders service Up and Running ...');
});